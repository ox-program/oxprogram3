/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.oxprogram3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author bwstx
 */
public class TDDTest {

    public TDDTest() {
    }
    //add(int a,int b) -> int
    //add(1,3)->3
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }
    //add(1,3)->3
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }
    
    //chup(char player1,char player2)-> "p1","p2"
    @Test
    public void testChup_p1_p_p2_p_is_draw() {
        assertEquals("draw", Example.chup('p','p'));
    }
    
    @Test
    public void testChup_p1_h_p2_h_is_draw() {
        assertEquals("draw", Example.chup('h','h'));
    }
    
    @Test
    public void testChup_p1_s_p2_s_is_draw() {
        assertEquals("draw", Example.chup('s','s'));
    }
    
    @Test
    public void testChup_p1_s_p2_p_is_p1() {
        assertEquals("p1" ,Example.chup('s','p'));
    }
    @Test
    public void testChup_p1_p_p2_h_is_p1() {
        assertEquals("p1" ,Example.chup('p','h'));
    }
    @Test
    public void testChup_p1_h_p2_s_is_p1() {
        assertEquals("p1" ,Example.chup('h','s'));
    }
    
    @Test
    public void testChup_p1_h_p2_p_is_p2() {
        assertEquals("p2" ,Example.chup('h','p'));
    }
    @Test
    public void testChup_p1_s_p2_h_is_p2() {
        assertEquals("p2" ,Example.chup('s','h'));
    }
    @Test
    public void testChup_p1_p_p2_s_is_p2() {
        assertEquals("p2" ,Example.chup('p','s'));
    }
}