/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.oxprogram3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author bwstx
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @Test
    public void testCheckVerticalPlayerOCol1Win() {
        char[][] table = {{'O', '-', '-'},
                                 {'O', '-', '-'},
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol2Win() {
        char[][] table = {{'-', 'O', '-'},
                                 {'-', 'O', '-'},
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerOCol3Win() {
        char[][] table = {{'-', '-', 'O'},
                                {'-', '-', 'O'},
                                {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
    public void testCheckVerticalPlayerXCol1Win() {
        char[][] table = {{'X', '-', '-'},
                                 {'X', '-', '-'},
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol2Win() {
        char[][] table = {{'-', 'X', '-'},
                                 {'-', 'X', '-'},
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticalPlayerXCol3Win() {
        char[][] table = {{'-', '-', 'X'},
                                {'-', '-', 'X'},
                                {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckHorizontalPlayerORol1Win() {
        char[][] table = {{'O', 'O', 'O'},
                                 {'-', '-', '-'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORow2Win() {
        char[][] table = {{'-', '-', '-'},
                                 {'O', 'O', 'O'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerORol3Win() {
        char[][] table = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    @Test
    public void testCheckHorizontalPlayerXRol1Win() {
        char[][] table = {{'X', 'X', 'X'},
                                 {'-', '-', '-'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRow2Win() {
        char[][] table = {{'-', '-', '-'},
                                 {'X', 'X', 'X'},
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 2;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

    @Test
    public void testCheckHorizontalPlayerXRol3Win() {
        char[][] table = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int row = 3;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
    
    
    @Test
    public void testCheckX1OWin() {
        char[][] table = {{'O', '-', '-'},
                                 {'-', 'O', '-'},
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }

    @Test
    public void testCheckX1XWin() {
        char[][] table = {{'X', '-', '-'},
                                 {'-', 'X', '-'},
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
    }
    
    @Test
    public void testCheckX2OWin() {
        char[][] table = {{'-', '-', 'O'},
                                 {'-', 'O', '-'},
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }

    @Test
    public void testCheckX2XWin() {
        char[][] table = {{'-', '-', 'X'},
                                 {'-', 'X', '-'},
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
    }
    
      @Test
    public void testCheckDraw() {
        int count = 8;
        assertEquals(true, OXProgram.checkDraw(count));
    }
}
