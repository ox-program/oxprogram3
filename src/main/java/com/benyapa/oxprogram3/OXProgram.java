/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.oxprogram3;

import java.util.Scanner;

/**
 *
 * @author bwstx
 */
public class OXProgram {

    static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count =0;
    

    public static void main(String[] args) {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();

            if (finish) {
                break;
            }
        }

    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j]);
            }
            System.out.println();
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row, col: ");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(table, currentPlayer, row, col)) {
                finish = true;
                showTable();
                System.out.println(">>>" + currentPlayer + " Win<<<");
                return;
            }
            switchPlayer();
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean checkWin(char[][] table, char currentPlayer, int row, int col) {
        if (checkVertical(table, currentPlayer, col)) {
            return true;
        } else if (checkHorizontal(table, currentPlayer, row)) {
            return true;
        } else if (checkX(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table, char currentPlayer, int col) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] table, char currentPlayer, int row) {
        for (int j = 0; j < table.length; j++) {
            if (table[row - 1][j] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX(char[][] table, char currentPlayer) {
        if (checkX1(table, currentPlayer)) {
            return true;
        } else if (checkX2(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table, char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table, char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(int count) {
        if (count == 8) {
            return true;
        }
        return false;
    }
}
